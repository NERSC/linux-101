#!/bin/bash

# print each command and exit immediately if command has non-zero exit status
set -xe

# navigating through filesystem examples
pwd
cd
ls -la
cat .profile
mkdir files
cd files/
pwd
cd /root/files
touch names.txt
cat names.txt
mv names.txt countries.txt
touch a.txt b.txt c.txt
rm a.txt
mkdir names countries languages
touch countries/{usa,canada}.txt names/{richard,mike}.txt languages/{english,french,spanish}.txt
ls -lR
mkdir numbers
touch numbers/f{0..9}.txt
ls -l numbers
echo "My name is Richard"
echo "My name is Richard" > names/richard.txt 
cat names/richard.txt 
echo "My age is 35" > names/richard.txt
cat names/richard.txt 
> names/richard.txt 
cat names/richard.txt
echo "My name is Richard" > names/richard.txt
echo "My age is 35" >> names/richard.txt
cat names/richard.txt 
ls -l /dev/std*

# commands below need to run with 'set +e' since they are non-zero commands 
set +e
# input, stdout, error stream examples
ls -l /home/user
ls -l /home/user 1> /tmp/stdout.txt
cat /tmp/stdout.txt
ls -l /home/user 2> /tmp/stderr.txt
ls /xyz 2>/dev/null
cat /dev/null

set -e

# piping output examples
rm -rf /root/files
cat ~/.profile | tail -n 1
tail -n 1 ~/.profile 

# permissions examples
cd 
touch permission.txt
ls -l permission.txt
chmod 000 permission.txt 
ls -l permission.txt 
chmod 644 permission.txt 
ls -l permission.txt 
chmod g+w permission.txt
ls -l permission.txt 
chmod u=rwx permission.txt 
ls -l permission.txt 
chmod o-r permission.txt
ls -l permission.txt
chmod 000 permission.txt
ls -l permission.txt
chmod a+r permission.txt
ls -l permission.txt
umask 
mkdir new_dir
touch new_file
ls -ld new_dir new_file

# PATH example
echo $PATH
which cat
export PATH=''

# command below will exit
set +e
ls
set -e
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ls

# process management
sleep 5
sleep 60 &
jobs

sleep 360 &
sleep 360 &
pkill -e sleep
#time sleep 10 &
#fg 1

apt install -y psmisc
pstree -ps 1

# command history example
# enable 'history' command inside shell script
set -o history
history 10
# !cat
export HISTTIMEFORMAT="%F %T "
history 2

# find examples
find $HOME
find /etc -type d | wc -l
find $HOME -type f -name ".bash*"
find / -type c -exec ls -l {} \;
find /bin -type l -exec ls -l {} \;
find /var -size 10k  
find /var -size 10k -exec ls -l {} \;


touch ~/a ~/A
find $HOME -name a
find $HOME -iname a
find .  -type f -perm 0644 -exec ls -l {} \;
find .  -type f ! -perm 0644 -exec ls -l {} \;
find . -type f -empty
mkdir dir{1..6}
find . -type d -empty 
find /var -mtime 1 -exec ls -l {} \;
find $HOME -empty -name "dir*" | wc -l
find $HOME -empty -name "dir*" -delete
find $HOME -empty -name "dir*" | wc -l

apt install -y man
unminimize
which man
man -f ls
